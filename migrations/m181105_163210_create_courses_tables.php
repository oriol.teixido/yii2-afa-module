<?php

use yii\db\Migration;

class m181105_163210_create_courses_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%afa_courses}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
        ]);
        $this->createTable('{{%afa_course_groups}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'course_id' =>  $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-course_groups-courses',
            '{{%afa_course_groups}}',
            'course_id',
            '{{%afa_courses}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%afa_course_groups}}');
        $this->dropTable('{{%afa_courses}}');
        return true;
    }
}
