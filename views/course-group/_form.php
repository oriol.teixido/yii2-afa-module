<?php
use yii\bootstrap\ActiveForm;

use oteixido\gui\widgets\buttons\DeleteButtonWidget;
use oteixido\gui\widgets\buttons\SubmitButtonWidget;
use oteixido\gui\widgets\ModelActionsWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">
    <?= ModelActionsWidget::widget(['model' => $model]) ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'course_id')->hiddenInput()->label(false); ?>
    <div class="form-group">
        <?= SubmitButtonWidget::widget(); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
