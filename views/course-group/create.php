<?php

/* @var $this yii\web\View */
/* @var $owner app\models\Course */
/* @var $model app\models\CourseGroup */

$this->title = Yii::t('oteixido/afa', 'Crear grup');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/afa', 'Cursos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $owner->name, 'url' => ['update', 'id' => $owner->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-group-create">
    <?= $this->render('_form', [
        'owner' => $owner,
        'model' => $model,
    ]) ?>
</div>
