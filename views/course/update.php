<?php

/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = Yii::t('oteixido/afa', 'Modificar');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/afa', 'Cursos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row experiment-update">
    <div class="col-md-8">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
    <div class="col-md-4">
        <?= $this->render('_groups', [
            'model' => $model,
        ]) ?>
    </div>
</div>
