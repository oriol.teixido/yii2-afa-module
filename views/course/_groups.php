<?php
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

use oteixido\gui\widgets\ActionsWidget;
use oteixido\gui\components\ModelActionColumn;
use oteixido\gui\widgets\buttons\AddButtonWidget;

/* @var $this yii\web\View */
/* @var $model oteixido\afa\Course */
?>

<strong><?= Yii::t('oteixido/afa', 'Grups') ?></strong>

<?= ActionsWidget::widget(['actions' => [
        AddButtonWidget::widget(['controllerId' => 'course-group', 'foreignKey' => [ 'course_id' => $model->id ]])
    ]
]) ?>

<?= GridView::widget([
    'dataProvider' => new ActiveDataProvider([
        'query' => $model->getGroups(),
        'pagination' => false,
        'sort' => [
            'defaultOrder' => [
                'name' => SORT_ASC,
            ]
        ],
    ]),
    'columns' => [
        'name',
        [ 'class' => ModelActionColumn::className(), 'controller' => 'course-group' ],
    ],
]); ?>
