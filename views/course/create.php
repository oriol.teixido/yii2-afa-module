<?php

/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = Yii::t('oteixido/afa', 'Crear');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oteixido/afa', 'Cursos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
