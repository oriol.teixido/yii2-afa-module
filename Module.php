<?php

namespace oteixido\afa;

use yii\i18n\PhpMessageSource;
use Yii;

/**
 * yii2-afa module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'oteixido\afa\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->i18n->translations['oteixido/afa'] = [
            'class' => PhpMessageSource::className(),
            'sourceLanguage' => 'ca-ES',
            'basePath' => '@app/vendor/oteixido/yii2-afa-module/messages'
        ];
    }
}
