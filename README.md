Yii2 AFA Module
================
Yii2 AFA Module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist oteixido/yii2-afa-module "*"
```

or add

```
"oteixido/yii2-afa-module": "*"
```

to the require section of your `composer.json` file.


Migrations
----------

```bash
./yii migrate --migrationPath=vendor/oteixido/yii2-bank-module/migrations/
./yii migrate --migrationPath=vendor/oteixido/yii2-afa-module/migrations/
```
