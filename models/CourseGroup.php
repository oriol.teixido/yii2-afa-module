<?php

namespace oteixido\afa\models;

use Yii;

/**
 * This is the model class for table "course_groups".
 *
 * @property int $id
 * @property string $name
 * @property int $course_id
 *
 * @property Course[] $course
 */
class CourseGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%afa_course_groups}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 64],
            ['name', 'trim'],
            ['course_id', 'required'],
            ['course_id', 'exist', 'targetClass' => Course::class, 'targetAttribute' => ['course_id' => 'id']],
            [['name', 'course_id'], 'unique', 'targetAttribute' => ['name', 'course_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }
}
